package bazar666.uraniumclicker

import java.math.BigDecimal
import bazar666.uraniumclicker.R

class WorkerListSingleton {
      var workers_table: ArrayList<Worker> =ArrayList<Worker>()
    val worker_1 = Worker(1, "Laser Canon", BigDecimal(1), BigDecimal(100), BigDecimal(50), BigDecimal(1000), R.drawable.lasercanon)
    val worker_2 = Worker(2, "Light Chaser", BigDecimal("5"), BigDecimal(10000), BigDecimal(650), BigDecimal(1000), R.drawable.lightchaser)
    val worker_3 = Worker(3, "Wolverun 13", BigDecimal(10), BigDecimal(100000), BigDecimal(79900), BigDecimal(1000), R.drawable.wolverun13)
    val worker_4 = Worker(4, "Alien Destroyer", BigDecimal(15), BigDecimal(1000000), BigDecimal(660000), BigDecimal(1000), R.drawable.aliendestroyer)
    val worker_5 = Worker(5, "Goat Ship", BigDecimal(20), BigDecimal(100000000), BigDecimal(7760000000), BigDecimal(1000), R.drawable.goatship)
    val worker_6 = Worker(6, "Heavy Warship", BigDecimal(25), BigDecimal(1000000000), BigDecimal(550000000), BigDecimal(1000), R.drawable.heavywarship)
    val worker_7 = Worker(7, "Plasma Chaser", BigDecimal(30), BigDecimal(10000000000), BigDecimal(4000000000), BigDecimal(1000), R.drawable.plasmachaser)
    val worker_8 = Worker(8, "White Star", BigDecimal(35), BigDecimal(100000000000), BigDecimal(50000000000), BigDecimal(1000), R.drawable.whitestar)


    val  workers_table_const = arrayListOf<Worker>(

                    worker_1,
                    worker_2,
                    worker_3,
                    worker_4,
                    worker_5,
                    worker_6,
                    worker_7,
                    worker_8)

    companion object {

        val instance: WorkerListSingleton  by lazy{ WorkerListSingleton() }
    }



     fun add_initial_workers(worker: Worker) {

         if(this.workers_table.size==0) {
             this.workers_table.add(worker)
         }
     }

    fun add_workers(position:Int) {

        if((position==this.workers_table.size)&&(this.workers_table_const.size > position)) {
            this.workers_table.add(this.workers_table_const[position])
        }
    }

    fun update_workers_table_from_DB(dbhandler : MyDBHandler){
        var worker: Worker

        val data = dbhandler.getListContents()
        val numRows = data.count

        if (numRows==0){

        }else{

            var x =0
            while (x < this.workers_table.size){
                this.workers_table.removeAt(x)
                x++
            }

            var i = 0;
            while(data.moveToNext()){
                worker = Worker(data.getString(0).toInt(),
                        data.getString(1),
                        BigDecimal(data.getString(2)),
                        BigDecimal(data.getString(3)),
                        BigDecimal(data.getString(4)),
                        BigDecimal(data.getString(5)),
                        this.workers_table_const[i].ship_image)
                worker.amount =  BigDecimal(data.getString(6))
                worker.income_cash_mod =  BigDecimal(data.getString(7))

                this.workers_table.add(i,worker)
                ++i


            }

        }
    }

    fun update_workers_to_DB(dbhandler : MyDBHandler){

        var i = 0;

        for(worker in this.workers_table){
            dbhandler.addWorker(worker)
            dbhandler.updateWorker(worker,i)
            i++
        }

    }


}