package bazar666.uraniumclicker

import android.annotation.SuppressLint
import android.graphics.drawable.AnimationDrawable
import java.math.BigDecimal
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main2.*
import android.widget.Toast
import android.os.CountDownTimer
import android.util.Log
import android.widget.ProgressBar
import org.w3c.dom.Text


class Main2Activity : AppCompatActivity() {
    var dbhandler = MyDBHandler(this ,null, null, 1)
    val handler = Handler()
    val handler1 = Handler()
    val workers_table = WorkerListSingleton.instance.workers_table
    val company = Company.instance

    //val  researches_table = ResearchesListSingleton.instance.research_table



    val runnable = object : Runnable {
        override fun run() {

            show_company(company)

            handler.postDelayed(this, 300)

        }
    }



    val runnable1 = object : Runnable {
        override fun run() {

            refresh_cash(company,WorkerListSingleton.instance.workers_table)

            handler.postDelayed(this, 1000)

        }
    }

    fun show_company(company:Company){
        editTextCASH.setKeyListener(null);
        editTextCPS.setKeyListener(null);
        TextCPS.setKeyListener(null);
        TextCPS.setKeyListener(null);

        show_value_text(company.cash, editTextCASH)
        show_value_text(company.cash_per_sec, editTextCPS)

    }

    fun show_value_text(cash: BigDecimal, textView: TextView){
        var cash_tmp=cash

        if (cash >= BigDecimal(1000000000000)) {
            textView.text = cash_tmp.divide(BigDecimal("1000000000000"),2,BigDecimal.ROUND_DOWN).toString() +"B"

        }else if (cash >= BigDecimal(1000000000)) {
            textView.text = cash_tmp.divide(BigDecimal("1000000000"),2,BigDecimal.ROUND_DOWN).toString() +"Mi"


        }else if (cash >= BigDecimal(1000000)) {
            textView.text = cash_tmp.divide(BigDecimal("1000000"),2,BigDecimal.ROUND_DOWN).toString() +"M"


        }else if (cash >= BigDecimal(1000)) {
            textView.setText(cash_tmp.divide(BigDecimal("1000.00"),2,BigDecimal.ROUND_DOWN).toString() +"K")

        }else
            textView.setText(cash_tmp.setScale(2,BigDecimal.ROUND_DOWN).toString())
    }

    fun refresh_cash(company: Company, workers_table: ArrayList<Worker>){
        var i=0

        for (item in workers_table) {
            company.cash = company.cash.add(workers_table[i].income_cash.multiply(workers_table[i].amount)).add(workers_table[i].income_cash.multiply(workers_table[i].amount).multiply(workers_table[i].income_cash_mod))

            i++
        }

    }



    val worker_1 = Worker(1,"Laser Canon", BigDecimal(1).setScale(2,BigDecimal.ROUND_DOWN), BigDecimal(100), BigDecimal(50), BigDecimal(1000),R.drawable.lasercanon)


val researches_1 = Researches(1,
        "Laser Canon",

        R.drawable.planet2,
        BigDecimal(0),
        BigDecimal(0.00).setScale(2,BigDecimal.ROUND_DOWN),
        "Photon",
        BigDecimal(10),
        BigDecimal(10),


        R.drawable.planet2,
        BigDecimal(0),
        BigDecimal(1.10).setScale(2,BigDecimal.ROUND_DOWN),
        "Carbon",
        BigDecimal(10),
        BigDecimal(10),

        R.drawable.planet2,
        BigDecimal(0),
        BigDecimal(1.10).setScale(2,BigDecimal.ROUND_DOWN),
        "V1",
        BigDecimal(10),
        BigDecimal(10),

        R.drawable.planet2,
        BigDecimal(0),
        BigDecimal(1.10).setScale(2,BigDecimal.ROUND_DOWN),
        "NitroV1",
        BigDecimal(10),
        BigDecimal(10),

        R.drawable.planet2,
        BigDecimal(0),
        BigDecimal(1.10).setScale(2,BigDecimal.ROUND_DOWN),
        "Laser",
        BigDecimal(10),
        BigDecimal(10)

)




    @SuppressLint("ResourceType", "PrivateResource")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        //INIT
        editTextCASH.setKeyListener(null);
        editTextCPS.setKeyListener(null);
        TextCPS.setKeyListener(null);
        TextCPS.setKeyListener(null);


//--------------------------------------------------------------------

        WorkerListSingleton.instance.add_initial_workers(worker_1)

        //ResearchesListSingleton.instance.add_initial_research(researches_1)

        dbhandler.addWorker(worker_1)
        //dbhandler.addWorker(worker_2)
        WorkerListSingleton.instance.workers_table =ArrayList<Worker>()
        WorkerListSingleton.instance.update_workers_table_from_DB(dbhandler)


        dbhandler.createCompany(company)
        company.update_company_from_DB(dbhandler)

        ResearchesListSingleton.instance.research_table =ArrayList<Researches>()
        ResearchesListSingleton.instance.update_researches_table_from_DB(dbhandler)
        show_company(company)



        fun update_workers(company:Company){

        }







//Start Loop Tasks

        handler1.postDelayed(runnable1, 1000);
        handler.postDelayed(runnable, 300);

        var image = findViewById<ImageView>(R.id.imageview_animation)
        image.setBackgroundResource(R.drawable.first_animation);
        (image.background as AnimationDrawable).start()
       //image.visibility=View.INVISIBLE
        image.setOnClickListener() {

            if (WorkerListSingleton.instance.workers_table[0].amount.equals(BigDecimal(0))){
                //Toast.makeText(this, WorkerListSingleton.instance.workers_table[0].amount.toString(),Toast.LENGTH_LONG).show()
                company.cash = company.cash.add(WorkerListSingleton.instance.workers_table[0].income_cash).add(WorkerListSingleton.instance.workers_table[0].income_cash.multiply(WorkerListSingleton.instance.workers_table[0].amount).multiply(WorkerListSingleton.instance.workers_table[0].income_cash_mod))
            }else{
                //Toast.makeText(this, "ttt",Toast.LENGTH_LONG).show()
                company.cash = company.cash.add((WorkerListSingleton.instance.workers_table[0].income_cash.multiply(WorkerListSingleton.instance.workers_table[0].amount)).add(WorkerListSingleton.instance.workers_table[0].income_cash.multiply(WorkerListSingleton.instance.workers_table[0].amount).multiply(WorkerListSingleton.instance.workers_table[0].income_cash_mod)))
            }


        }

        var shipanim1 = findViewById<ImageView>(R.id.ship_anim1)
        shipanim1.setBackgroundResource(R.drawable.ship_anim1_animation);
        shipanim1.visibility= View.INVISIBLE
        shipanim1.isEnabled= false
        var shipshot1 = findViewById<ImageView>(R.id.ship_shot1)
        shipshot1.setBackgroundResource(R.drawable.ship_shot1_animation);
        shipshot1.visibility= View.INVISIBLE
        shipshot1.isEnabled= false
        val progressBar1: ProgressBar
        progressBar1 = findViewById(R.id.progressBar1)
        progressBar1.visibility=View.INVISIBLE

        var shipanim2 = findViewById<ImageView>(R.id.ship_anim2)
        shipanim2.setBackgroundResource(R.drawable.ship_anim2_animation);
        shipanim2.visibility= View.INVISIBLE
        shipanim2.isEnabled= false
        var shipshot2 = findViewById<ImageView>(R.id.ship_shot2)
        shipshot2.setBackgroundResource(R.drawable.ship_shot2_animation);
        shipshot2.visibility= View.INVISIBLE
        shipshot2.isEnabled= false
        val progressBar2: ProgressBar
        progressBar2 = findViewById(R.id.progressBar2)
        progressBar2.visibility=View.INVISIBLE

        var shipanim3 = findViewById<ImageView>(R.id.ship_anim3)
        shipanim3.setBackgroundResource(R.drawable.ship_anim3_animation);
        shipanim3.visibility= View.INVISIBLE
        shipanim3.isEnabled= false
        var shipshot3 = findViewById<ImageView>(R.id.ship_shot3)
        shipshot3.setBackgroundResource(R.drawable.ship_shot3_animation);
        shipshot3.visibility= View.INVISIBLE
        shipshot3.isEnabled= false
        val progressBar3: ProgressBar
        progressBar3 = findViewById(R.id.progressBar3)
        progressBar3.visibility=View.INVISIBLE

        var shipanim4 = findViewById<ImageView>(R.id.ship_anim4)
        shipanim4.setBackgroundResource(R.drawable.ship_anim4_animation);
        shipanim4.visibility= View.INVISIBLE
        shipanim4.isEnabled= false
        var shipshot4 = findViewById<ImageView>(R.id.ship_shot4)
        shipshot4.setBackgroundResource(R.drawable.ship_shot4_animation);
        shipshot4.visibility= View.INVISIBLE
        shipshot4.isEnabled= false
        val progressBar4: ProgressBar
        progressBar4 = findViewById(R.id.progressBar4)
        progressBar4.visibility=View.INVISIBLE

        var shipanim5 = findViewById<ImageView>(R.id.ship_anim5)
        shipanim5.setBackgroundResource(R.drawable.ship_anim5_animation);
        shipanim5.visibility= View.INVISIBLE
        shipanim5.isEnabled= false
        var shipshot5 = findViewById<ImageView>(R.id.ship_shot5)
        shipshot5.setBackgroundResource(R.drawable.ship_shot5_animation);
        shipshot5.visibility= View.INVISIBLE
        shipshot5.isEnabled= false
        val progressBar5: ProgressBar
        progressBar5 = findViewById(R.id.progressBar5)
        progressBar5.visibility=View.INVISIBLE

        var shipanim6 = findViewById<ImageView>(R.id.ship_anim6)
        shipanim6.setBackgroundResource(R.drawable.ship_anim6_animation);
        shipanim6.visibility= View.INVISIBLE
        shipanim6.isEnabled= false
        var shipshot6 = findViewById<ImageView>(R.id.ship_shot6)
        shipshot6.setBackgroundResource(R.drawable.ship_shot6_animation);
        shipshot6.visibility= View.INVISIBLE
        shipshot6.isEnabled= false
        val progressBar6: ProgressBar
        progressBar6 = findViewById(R.id.progressBar6)
        progressBar6.visibility=View.INVISIBLE

        var shipanim7 = findViewById<ImageView>(R.id.ship_anim7)
        shipanim7.setBackgroundResource(R.drawable.ship_anim7_animation);
        shipanim7.visibility= View.INVISIBLE
        shipanim7.isEnabled= false
        var shipshot7 = findViewById<ImageView>(R.id.ship_shot7)
        shipshot7.setBackgroundResource(R.drawable.ship_shot7_animation);
        shipshot7.visibility= View.INVISIBLE
        shipshot7.isEnabled= false
        val progressBar7: ProgressBar
        progressBar7 = findViewById(R.id.progressBar7)
        progressBar7.visibility=View.INVISIBLE

        var shipanim8 = findViewById<ImageView>(R.id.ship_anim8)
        shipanim8.setBackgroundResource(R.drawable.ship_anim8_animation);
        shipanim8.visibility= View.INVISIBLE
        shipanim8.isEnabled= false
        var shipshot8 = findViewById<ImageView>(R.id.ship_shot8)
        shipshot8.setBackgroundResource(R.drawable.ship_shot8_animation);
        shipshot8.visibility= View.INVISIBLE
        shipshot8.isEnabled= false
        val progressBar8: ProgressBar
        progressBar8 = findViewById(R.id.progressBar8)
        progressBar8.visibility=View.INVISIBLE

        object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                Log.d("log", (millisUntilFinished / 1000).toString())
                progressBar1.setProgress((millisUntilFinished / 1000).toInt());
                progressBar2.setProgress((millisUntilFinished / 1000).toInt());
                progressBar3.setProgress((millisUntilFinished / 1000).toInt());
                progressBar4.setProgress((millisUntilFinished / 1000).toInt());
                progressBar5.setProgress((millisUntilFinished / 1000).toInt());
                progressBar6.setProgress((millisUntilFinished / 1000).toInt());
                progressBar7.setProgress((millisUntilFinished / 1000).toInt());
                progressBar8.setProgress((millisUntilFinished / 1000).toInt());

                for (i  in 0..WorkerListSingleton.instance.workers_table.size-1){
                    if(i==0){

                        if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))){
                            progressBar1.visibility= View.VISIBLE
                        }

                    }

                    if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))){
                        if(i==1) {
                            progressBar2.visibility = View.VISIBLE
                        }
                    }

                    if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))){
                        if(i==2) {
                            progressBar3.visibility = View.VISIBLE
                        }
                    }
                    if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))){
                        if(i==3) {
                            progressBar4.visibility = View.VISIBLE
                        }
                    }
                    if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))){
                        if(i==4) {
                            progressBar5.visibility = View.VISIBLE
                        }
                    }
                    if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))){
                        if(i==5) {
                            progressBar6.visibility = View.VISIBLE
                        }
                    }
                    if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))){
                        if(i==6) {
                            progressBar7.visibility = View.VISIBLE
                        }
                    }
                    if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))){
                        if(i==7) {
                            progressBar8.visibility = View.VISIBLE
                        }
                    }
                }
         }

            override fun onFinish() {
                // called after count down is finished
                shipanim1.isEnabled = true
                shipanim2.isEnabled = true
                shipanim3.isEnabled = true
                shipanim4.isEnabled = true
                shipanim5.isEnabled = true
                shipanim6.isEnabled = true
                shipanim7.isEnabled = true
                shipanim8.isEnabled = true
            }
        }.start()

        val shipanim_list : ArrayList<ImageView> =ArrayList<ImageView>()
        shipanim_list.add(shipanim1)
        shipanim_list.add(shipanim2)
        shipanim_list.add(shipanim3)
        shipanim_list.add(shipanim4)
        shipanim_list.add(shipanim5)
        shipanim_list.add(shipanim6)
        shipanim_list.add(shipanim7)
        shipanim_list.add(shipanim8)

        val ship_shot_anim_list : ArrayList<ImageView> =ArrayList<ImageView>()
        ship_shot_anim_list.add(shipshot1)
        ship_shot_anim_list.add(shipshot2)
        ship_shot_anim_list.add(shipshot3)
        ship_shot_anim_list.add(shipshot4)
        ship_shot_anim_list.add(shipshot5)
        ship_shot_anim_list.add(shipshot6)
        ship_shot_anim_list.add(shipshot7)
        ship_shot_anim_list.add(shipshot8)

        val progressBar_list : ArrayList<ProgressBar> =ArrayList<ProgressBar>()
        progressBar_list.add(progressBar1)
        progressBar_list.add(progressBar2)
        progressBar_list.add(progressBar3)
        progressBar_list.add(progressBar4)
        progressBar_list.add(progressBar5)
        progressBar_list.add(progressBar6)
        progressBar_list.add(progressBar7)
        progressBar_list.add(progressBar8)

        shipanim1.setOnClickListener() {

            if ((shipanim1.isEnabled)&&(WorkerListSingleton.instance.workers_table[0].amount>=BigDecimal(100))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim1.isEnabled=false

                (shipanim_list[0].background as AnimationDrawable).stop()
                (shipanim_list[0].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())

                                progressBar1.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim1.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[0].income_cash).add(WorkerListSingleton.instance.workers_table[0].income_cash.multiply( WorkerListSingleton.instance.workers_table[0].income_cash_mod)).multiply(BigDecimal(10)))

            }
            else if ((shipanim1.isEnabled)&&(WorkerListSingleton.instance.workers_table[0].amount>=BigDecimal(75))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim1.isEnabled=false

                (shipanim_list[0].background as AnimationDrawable).stop()
                (shipanim_list[0].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar1.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim1.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[0].income_cash).add(WorkerListSingleton.instance.workers_table[0].income_cash.multiply( WorkerListSingleton.instance.workers_table[0].income_cash_mod)).multiply(BigDecimal(7.50)))

            }
           else if ((shipanim1.isEnabled)&&(WorkerListSingleton.instance.workers_table[0].amount>=BigDecimal(50))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim1.isEnabled=false
                (shipanim_list[0].background as AnimationDrawable).stop()
                (shipanim_list[0].background as AnimationDrawable).setVisible(true,true)
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar1.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim1.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[0].income_cash).add(WorkerListSingleton.instance.workers_table[0].income_cash.multiply( WorkerListSingleton.instance.workers_table[0].income_cash_mod)).multiply(BigDecimal(5)))

            }
            else if ((shipanim1.isEnabled)&&(WorkerListSingleton.instance.workers_table[0].amount>=BigDecimal(25))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim1.isEnabled=false
                (shipanim_list[0].background as AnimationDrawable).stop()
                (shipanim_list[0].background as AnimationDrawable).setVisible(true,true)

                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar1.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim1.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[0].income_cash).add(WorkerListSingleton.instance.workers_table[0].income_cash.multiply( WorkerListSingleton.instance.workers_table[0].income_cash_mod)).multiply(BigDecimal(2.50)))

            }
        }

        shipanim2.setOnClickListener() {
            if ((shipanim2.isEnabled)&&(WorkerListSingleton.instance.workers_table[1].amount>=BigDecimal(100))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim2.isEnabled=false

                (shipanim_list[1].background as AnimationDrawable).stop()
                (shipanim_list[1].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())

                        progressBar2.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim2.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[1].income_cash).add(WorkerListSingleton.instance.workers_table[1].income_cash.multiply( WorkerListSingleton.instance.workers_table[1].income_cash_mod)).multiply(BigDecimal(10)))

            }
            else if ((shipanim2.isEnabled)&&(WorkerListSingleton.instance.workers_table[1].amount>=BigDecimal(75))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim2.isEnabled=false

                (shipanim_list[1].background as AnimationDrawable).stop()
                (shipanim_list[1].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar2.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim2.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[1].income_cash).add(WorkerListSingleton.instance.workers_table[1].income_cash.multiply( WorkerListSingleton.instance.workers_table[1].income_cash_mod)).multiply(BigDecimal(7.5)))

            }
            else if ((shipanim2.isEnabled)&&(WorkerListSingleton.instance.workers_table[1].amount>=BigDecimal(50))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim2.isEnabled=false
                (shipanim_list[1].background as AnimationDrawable).stop()
                (shipanim_list[1].background as AnimationDrawable).setVisible(true,true)
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar2.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim2.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[1].income_cash).add(WorkerListSingleton.instance.workers_table[1].income_cash.multiply( WorkerListSingleton.instance.workers_table[1].income_cash_mod)).multiply(BigDecimal(5)))

            }
            else if ((shipanim2.isEnabled)&&(WorkerListSingleton.instance.workers_table[1].amount>=BigDecimal(25))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim2.isEnabled=false
                (shipanim_list[1].background as AnimationDrawable).stop()
                (shipanim_list[1].background as AnimationDrawable).setVisible(true,true)

                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar2.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim2.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[1].income_cash).add(WorkerListSingleton.instance.workers_table[1].income_cash.multiply( WorkerListSingleton.instance.workers_table[1].income_cash_mod)).multiply(BigDecimal(2.5)))

            }
        }

        shipanim3.setOnClickListener() {
            if ((shipanim3.isEnabled)&&(WorkerListSingleton.instance.workers_table[2].amount>=BigDecimal(100))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim3.isEnabled=false

                (shipanim_list[2].background as AnimationDrawable).stop()
                (shipanim_list[2].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())

                        progressBar3.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim3.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[2].income_cash).add(WorkerListSingleton.instance.workers_table[2].income_cash.multiply( WorkerListSingleton.instance.workers_table[2].income_cash_mod)).multiply(BigDecimal(10)))

            }
            else if ((shipanim3.isEnabled)&&(WorkerListSingleton.instance.workers_table[2].amount>=BigDecimal(75))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim3.isEnabled=false

                (shipanim_list[2].background as AnimationDrawable).stop()
                (shipanim_list[2].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar3.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim3.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[2].income_cash).add(WorkerListSingleton.instance.workers_table[2].income_cash.multiply( WorkerListSingleton.instance.workers_table[2].income_cash_mod)).multiply(BigDecimal(7.5)))

            }
            else if ((shipanim3.isEnabled)&&(WorkerListSingleton.instance.workers_table[2].amount>=BigDecimal(50))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim3.isEnabled=false
                (shipanim_list[2].background as AnimationDrawable).stop()
                (shipanim_list[2].background as AnimationDrawable).setVisible(true,true)
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar3.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim3.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[2].income_cash).add(WorkerListSingleton.instance.workers_table[2].income_cash.multiply( WorkerListSingleton.instance.workers_table[2].income_cash_mod)).multiply(BigDecimal(5)))

            }
            else if ((shipanim3.isEnabled)&&(WorkerListSingleton.instance.workers_table[2].amount>=BigDecimal(25))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim3.isEnabled=false
                (shipanim_list[2].background as AnimationDrawable).stop()
                (shipanim_list[2].background as AnimationDrawable).setVisible(true,true)

                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar3.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim3.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[2].income_cash).add(WorkerListSingleton.instance.workers_table[2].income_cash.multiply( WorkerListSingleton.instance.workers_table[2].income_cash_mod)).multiply(BigDecimal(2.5)))

            }
        }

        shipanim4.setOnClickListener() {
            if ((shipanim4.isEnabled)&&(WorkerListSingleton.instance.workers_table[3].amount>=BigDecimal(100))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim4.isEnabled=false

                (shipanim_list[3].background as AnimationDrawable).stop()
                (shipanim_list[3].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())

                        progressBar4.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim4.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[3].income_cash).add(WorkerListSingleton.instance.workers_table[3].income_cash.multiply( WorkerListSingleton.instance.workers_table[3].income_cash_mod)).multiply(BigDecimal(10)))

            }
            else if ((shipanim4.isEnabled)&&(WorkerListSingleton.instance.workers_table[3].amount>=BigDecimal(75))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim4.isEnabled=false

                (shipanim_list[3].background as AnimationDrawable).stop()
                (shipanim_list[3].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar4.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim4.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[3].income_cash).add(WorkerListSingleton.instance.workers_table[3].income_cash.multiply( WorkerListSingleton.instance.workers_table[3].income_cash_mod)).multiply(BigDecimal(7.5)))

            }
            else if ((shipanim4.isEnabled)&&(WorkerListSingleton.instance.workers_table[3].amount>=BigDecimal(50))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim4.isEnabled=false
                (shipanim_list[3].background as AnimationDrawable).stop()
                (shipanim_list[3].background as AnimationDrawable).setVisible(true,true)
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar4.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim4.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[3].income_cash).add(WorkerListSingleton.instance.workers_table[3].income_cash.multiply( WorkerListSingleton.instance.workers_table[3].income_cash_mod)).multiply(BigDecimal(5)))

            }
            else if ((shipanim4.isEnabled)&&(WorkerListSingleton.instance.workers_table[3].amount>=BigDecimal(25))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim4.isEnabled=false
                (shipanim_list[3].background as AnimationDrawable).stop()
                (shipanim_list[3].background as AnimationDrawable).setVisible(true,true)

                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar4.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim4.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[3].income_cash).add(WorkerListSingleton.instance.workers_table[3].income_cash.multiply( WorkerListSingleton.instance.workers_table[3].income_cash_mod)).multiply(BigDecimal(2.5)))

            }
        }

        shipanim5.setOnClickListener() {
            if ((shipanim5.isEnabled)&&(WorkerListSingleton.instance.workers_table[4].amount>=BigDecimal(100))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim5.isEnabled=false

                (shipanim_list[4].background as AnimationDrawable).stop()
                (shipanim_list[4].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())

                        progressBar5.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim5.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[4].income_cash).add(WorkerListSingleton.instance.workers_table[4].income_cash.multiply( WorkerListSingleton.instance.workers_table[4].income_cash_mod)).multiply(BigDecimal(10)))

            }
            else if ((shipanim5.isEnabled)&&(WorkerListSingleton.instance.workers_table[4].amount>=BigDecimal(75))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim5.isEnabled=false

                (shipanim_list[4].background as AnimationDrawable).stop()
                (shipanim_list[4].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar5.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim5.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[4].income_cash).add(WorkerListSingleton.instance.workers_table[4].income_cash.multiply( WorkerListSingleton.instance.workers_table[4].income_cash_mod)).multiply(BigDecimal(7.5)))

            }
            else if ((shipanim5.isEnabled)&&(WorkerListSingleton.instance.workers_table[4].amount>=BigDecimal(50))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim5.isEnabled=false
                (shipanim_list[4].background as AnimationDrawable).stop()
                (shipanim_list[4].background as AnimationDrawable).setVisible(true,true)
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar5.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim5.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(workers_table[4].income_cash).add(WorkerListSingleton.instance.workers_table[4].income_cash.multiply( WorkerListSingleton.instance.workers_table[4].income_cash_mod)).multiply(BigDecimal(5)))

            }
            else if ((shipanim5.isEnabled)&&(WorkerListSingleton.instance.workers_table[4].amount>=BigDecimal(25))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim5.isEnabled=false
                (shipanim_list[4].background as AnimationDrawable).stop()
                (shipanim_list[4].background as AnimationDrawable).setVisible(true,true)

                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar5.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim5.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[4].income_cash).add(WorkerListSingleton.instance.workers_table[4].income_cash.multiply( WorkerListSingleton.instance.workers_table[4].income_cash_mod)).multiply(BigDecimal(2.5)))

            }
        }

        shipanim6.setOnClickListener() {
            if ((shipanim6.isEnabled)&&(WorkerListSingleton.instance.workers_table[5].amount>=BigDecimal(100))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim6.isEnabled=false

                (shipanim_list[5].background as AnimationDrawable).stop()
                (shipanim_list[5].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())

                        progressBar6.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim6.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[5].income_cash).add(WorkerListSingleton.instance.workers_table[5].income_cash.multiply( WorkerListSingleton.instance.workers_table[5].income_cash_mod)).multiply(BigDecimal(10)))

            }
            else if ((shipanim6.isEnabled)&&(WorkerListSingleton.instance.workers_table[5].amount>=BigDecimal(75))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim6.isEnabled=false

                (shipanim_list[5].background as AnimationDrawable).stop()
                (shipanim_list[5].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar6.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim6.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[5].income_cash).add(WorkerListSingleton.instance.workers_table[5].income_cash.multiply( WorkerListSingleton.instance.workers_table[5].income_cash_mod)).multiply(BigDecimal(7.5)))

            }
            else if ((shipanim6.isEnabled)&&(WorkerListSingleton.instance.workers_table[5].amount>=BigDecimal(50))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim6.isEnabled=false
                (shipanim_list[5].background as AnimationDrawable).stop()
                (shipanim_list[5].background as AnimationDrawable).setVisible(true,true)
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar6.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim6.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[5].income_cash).add(WorkerListSingleton.instance.workers_table[5].income_cash.multiply( WorkerListSingleton.instance.workers_table[5].income_cash_mod)).multiply(BigDecimal(5)))

            }
            else if ((shipanim6.isEnabled)&&(WorkerListSingleton.instance.workers_table[5].amount>=BigDecimal(25))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim6.isEnabled=false
                (shipanim_list[5].background as AnimationDrawable).stop()
                (shipanim_list[5].background as AnimationDrawable).setVisible(true,true)

                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar6.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim6.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[5].income_cash).add(WorkerListSingleton.instance.workers_table[5].income_cash.multiply( WorkerListSingleton.instance.workers_table[5].income_cash_mod)).multiply(BigDecimal(2.5)))

            }
        }

        shipanim7.setOnClickListener() {
            if ((shipanim7.isEnabled)&&(WorkerListSingleton.instance.workers_table[6].amount>=BigDecimal(100))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim7.isEnabled=false

                (shipanim_list[6].background as AnimationDrawable).stop()
                (shipanim_list[6].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())

                        progressBar7.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim7.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[6].income_cash).add(WorkerListSingleton.instance.workers_table[6].income_cash.multiply( WorkerListSingleton.instance.workers_table[6].income_cash_mod)).multiply(BigDecimal(10)))

            }
            else if ((shipanim7.isEnabled)&&(WorkerListSingleton.instance.workers_table[6].amount>=BigDecimal(75))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim7.isEnabled=false

                (shipanim_list[6].background as AnimationDrawable).stop()
                (shipanim_list[6].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar7.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim7.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[6].income_cash).add(WorkerListSingleton.instance.workers_table[6].income_cash.multiply( WorkerListSingleton.instance.workers_table[6].income_cash_mod)).multiply(BigDecimal(7.5)))

            }
            else if ((shipanim7.isEnabled)&&(WorkerListSingleton.instance.workers_table[6].amount>=BigDecimal(50))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim7.isEnabled=false
                (shipanim_list[6].background as AnimationDrawable).stop()
                (shipanim_list[6].background as AnimationDrawable).setVisible(true,true)
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar7.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim7.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[6].income_cash).add(WorkerListSingleton.instance.workers_table[6].income_cash.multiply( WorkerListSingleton.instance.workers_table[6].income_cash_mod)).multiply(BigDecimal(5)))

            }
            else if ((shipanim7.isEnabled)&&(WorkerListSingleton.instance.workers_table[6].amount>=BigDecimal(25))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim7.isEnabled=false
                (shipanim_list[6].background as AnimationDrawable).stop()
                (shipanim_list[6].background as AnimationDrawable).setVisible(true,true)

                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar7.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim7.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[6].income_cash).add(WorkerListSingleton.instance.workers_table[6].income_cash.multiply( WorkerListSingleton.instance.workers_table[6].income_cash_mod)).multiply(BigDecimal(2.5)))

            }
        }

        shipanim8.setOnClickListener() {
            if ((shipanim8.isEnabled)&&(WorkerListSingleton.instance.workers_table[7].amount>=BigDecimal(100))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim8.isEnabled=false

                (shipanim_list[7].background as AnimationDrawable).stop()
                (shipanim_list[7].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())

                        progressBar8.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim8.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[7].income_cash).add(WorkerListSingleton.instance.workers_table[7].income_cash.multiply( WorkerListSingleton.instance.workers_table[7].income_cash_mod)).multiply(BigDecimal(10)))

            }
            else if ((shipanim8.isEnabled)&&(WorkerListSingleton.instance.workers_table[7].amount>=BigDecimal(75))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim8.isEnabled=false

                (shipanim_list[7].background as AnimationDrawable).stop()
                (shipanim_list[7].background as AnimationDrawable).setVisible(true,true)
                //shipanim_list[0].background = null
                //shipanim_list[0].setBackgroundResource(R.drawable.ship_anim1_animation);
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar8.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim8.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[7].income_cash).add(WorkerListSingleton.instance.workers_table[7].income_cash.multiply( WorkerListSingleton.instance.workers_table[7].income_cash_mod)).multiply(BigDecimal(7.5)))

            }
            else if ((shipanim8.isEnabled)&&(WorkerListSingleton.instance.workers_table[7].amount>=BigDecimal(50))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim8.isEnabled=false
                (shipanim_list[7].background as AnimationDrawable).stop()
                (shipanim_list[7].background as AnimationDrawable).setVisible(true,true)
                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar8.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim8.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[7].income_cash).add(WorkerListSingleton.instance.workers_table[7].income_cash.multiply( WorkerListSingleton.instance.workers_table[7].income_cash_mod)).multiply(BigDecimal(5)))

            }
            else if ((shipanim8.isEnabled)&&(WorkerListSingleton.instance.workers_table[7].amount>=BigDecimal(25))) {
                Toast.makeText(applicationContext, "cosmic power AcTiVaTeD",
                        Toast.LENGTH_LONG).show()
                shipanim8.isEnabled=false
                (shipanim_list[7].background as AnimationDrawable).stop()
                (shipanim_list[7].background as AnimationDrawable).setVisible(true,true)

                object : CountDownTimer(30000, 1000) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.d("log", (millisUntilFinished / 1000).toString())
                        progressBar8.setProgress((millisUntilFinished / 1000).toInt());
                    }

                    override fun onFinish() {
                        // called after count down is finished
                        shipanim8.isEnabled=true
                    }
                }.start()
                company.cash=(company.cash.add(WorkerListSingleton.instance.workers_table[7].income_cash).add(WorkerListSingleton.instance.workers_table[7].income_cash.multiply( WorkerListSingleton.instance.workers_table[7].income_cash_mod)).multiply(BigDecimal(2.5)))

            }
        }

        val handler2 = Handler()

        val runnable2 = object : Runnable {
            override fun run() {
                var i =0
                while(( i < WorkerListSingleton.instance.workers_table.size)&&(shipanim_list.size>i) ){
                    if (WorkerListSingleton.instance.workers_table[i].amount > BigDecimal(0)) {

                        shipanim_list[i].visibility = View.VISIBLE
                        ship_shot_anim_list[i].visibility = View.VISIBLE
                        (ship_shot_anim_list[i].background as AnimationDrawable).start()

                        if((WorkerListSingleton.instance.workers_table[i].amount>=BigDecimal(25))&&(shipanim_list[i].isEnabled== true)){

                            (shipanim_list[i].background as AnimationDrawable).start()
                            progressBar_list[i].visibility= View.VISIBLE

                        }

                    }


                    i++
                }


                handler.postDelayed(this, 1000)

            }
        }


        handler2.postDelayed(runnable2, 1000);
        //-------------------------MENU-----------------------------
        toggleButton_SHIPS.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked == true) {
                toggleButton_research.isChecked = false
                // The toggle is enabled/checked
                supportFragmentManager.popBackStack()
                supportFragmentManager.popBackStack()

                val someFragment2 = Fragment2()
                val transaction = supportFragmentManager.beginTransaction()


                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                transaction.replace(R.id.fragment,someFragment2) // give your fragment container id in first parameter
                transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
                transaction.commit()


            } else {
                //super.onBackPressed()

                val someFragment1 = Fragment1()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left )
                transaction.replace(R.id.fragment,someFragment1) // give your fragment container id in first parameter

                transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
                transaction.commit()


            }

        }


        toggleButton_research.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked == true) {
                toggleButton_SHIPS.isChecked = false
                // The toggle is enabled/checked
                supportFragmentManager.popBackStack()
                supportFragmentManager.popBackStack()
                val someFragment3 = Fragment3()
                val transaction = supportFragmentManager.beginTransaction()


                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                transaction.replace(R.id.fragment,someFragment3) // give your fragment container id in first parameter
                transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
                transaction.commit()


            } else {
                //super.onBackPressed()

                val someFragment1 = Fragment1()
                val transaction = supportFragmentManager.beginTransaction()
                transaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left )
                transaction.replace(R.id.fragment,someFragment1) // give your fragment container id in first parameter

                transaction.addToBackStack(null)  // if written, this transaction will be added to backstack
                transaction.commit()


            }

        }
        toggleButton_SHIPS.isChecked = true
        toggleButton_research.isChecked = false


    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK)
            Toast.makeText(applicationContext, "back press locked by cosmic power",
                    Toast.LENGTH_LONG).show()

        supportFragmentManager.popBackStack()
        supportFragmentManager.popBackStack()
        toggleButton_SHIPS.isChecked = false
        handler.removeCallbacksAndMessages(null)
        handler1.removeCallbacksAndMessages(null)
        super.onBackPressed()
        super.onBackPressed()
        return true
        // Disable back button..............
    }

    override fun onPause() {
        WorkerListSingleton.instance.update_workers_to_DB(dbhandler)
        Company.instance.update_company_to_DB(dbhandler)
        ResearchesListSingleton.instance.update_researches_to_DB(dbhandler)
        super.onPause()

    }

    override fun onStop() {
        WorkerListSingleton.instance.update_workers_to_DB(dbhandler)
        Company.instance.update_company_to_DB(dbhandler)
        ResearchesListSingleton.instance.update_researches_to_DB(dbhandler)
        super.onStop()


    }

    override fun onDestroy() {
        WorkerListSingleton.instance.update_workers_to_DB(dbhandler)
        Company.instance.update_company_to_DB(dbhandler)
        ResearchesListSingleton.instance.update_researches_to_DB(dbhandler)
        super.onDestroy()


    }

}



