package bazar666.uraniumclicker

import java.math.BigDecimal
import android.media.Image
import android.widget.ImageView

class Researches(val _id: Int,
                 var ship_name : String,

                 var image_technology : Int,
                 var level_technology: BigDecimal,
                 var income_cash_mod_technology: BigDecimal,
                 var research_name_technology: String,
                 var price_mod_technology: BigDecimal,
                 var upgrade_price_technology: BigDecimal,


                 var image_body : Int,
                 var level_body: BigDecimal,
                 var income_cash_mod_body: BigDecimal,
                 var research_name_body: String,
                 var price_mod_body: BigDecimal,
                 var upgrade_price_body: BigDecimal,


                 var image_engine : Int,
                 var level_engine: BigDecimal,
                 var income_cash_mod_engine: BigDecimal,
                 var research_name_engine: String,
                 var price_mod_engine: BigDecimal,
                 var upgrade_price_engine: BigDecimal,

                 var image_turbo : Int,
                 var level_turbo: BigDecimal,
                 var income_cash_mod_turbo: BigDecimal,
                 var research_name_turbo: String,
                 var price_mod_turbo: BigDecimal,
                 var upgrade_price_turboe: BigDecimal,

                 var image_weapon : Int,
                 var level_weapon: BigDecimal,
                 var income_cash_mod_weapon: BigDecimal,
                 var research_name_weapon: String,
                 var price_mod_weapon: BigDecimal,
                 var upgrade_price_weapon: BigDecimal) {


}