package bazar666.uraniumclicker

import java.math.BigDecimal
import bazar666.uraniumclicker.R

class ResearchesListSingleton {
    var research_table: ArrayList<Researches> =ArrayList<Researches>()

    val researches_1 = Researches(1,
            "Laser Canon",

            R.drawable.res11,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Photon",
            BigDecimal(100),
            BigDecimal(100),


            R.drawable.res12,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Carbon",
            BigDecimal(200),
            BigDecimal(200),

            R.drawable.res13,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "V1",
            BigDecimal(300),
            BigDecimal(300),

            R.drawable.res14,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "NitroV1",
            BigDecimal(400),
            BigDecimal(400),

            R.drawable.res15,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Laser",
            BigDecimal(500),
            BigDecimal(500)

    )

    val researches_2 = Researches(1,
            "Light Chaser",

            R.drawable.res21,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Lumen",
            BigDecimal(1000),
            BigDecimal(1000),


            R.drawable.res22,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Titan",
            BigDecimal(2000),
            BigDecimal(2000),

            R.drawable.res23,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "R1",
            BigDecimal(3000),
            BigDecimal(3000),

            R.drawable.res24,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "NitroR1",
            BigDecimal(4000),
            BigDecimal(4000),

            R.drawable.res25,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "XLaser",
            BigDecimal(5000),
            BigDecimal(5000)

    )
//TODO
    val researches_3 = Researches(3,
        "Wolverun 13",

        R.drawable.res31,
        BigDecimal(0),
        BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
        "Steel",
        BigDecimal(10000),
        BigDecimal(10000),


        R.drawable.res32,
        BigDecimal(0),
        BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
        "Chrom",
        BigDecimal(20000),
        BigDecimal(20000),

        R.drawable.res33,
        BigDecimal(0),
        BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
        "WW",
        BigDecimal(30000),
        BigDecimal(30000),

        R.drawable.res34,
        BigDecimal(0),
        BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
        "NitroW1",
        BigDecimal(40000),
        BigDecimal(40000),

        R.drawable.res35,
        BigDecimal(0),
        BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
        "XBlade",
        BigDecimal(50000),
        BigDecimal(50000)

)

    val researches_4 = Researches(4,
            "Alien Destroyer3",

            R.drawable.res41,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "XGreen",
            BigDecimal(100000),
            BigDecimal(100000),


            R.drawable.res42,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Chitin",
            BigDecimal(200000),
            BigDecimal(200000),

            R.drawable.res43,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "S1",
            BigDecimal(300000),
            BigDecimal(300000),

            R.drawable.res44,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "NitroS1",
            BigDecimal(400000),
            BigDecimal(400000),

            R.drawable.res45,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "XPower",
            BigDecimal(500000),
            BigDecimal(500000)

    )

    val researches_5 = Researches(5,
            "Goat Ship",

            R.drawable.res51,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Horn",
            BigDecimal(1000000),
            BigDecimal(1000000),


            R.drawable.res52,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Tail",
            BigDecimal(2000000),
            BigDecimal(2000000),

            R.drawable.res53,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "G1",
            BigDecimal(3000000),
            BigDecimal(3000000),

            R.drawable.res54,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "NitroG1",
            BigDecimal(4000000),
            BigDecimal(4000000),

            R.drawable.res55,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "XHorn",
            BigDecimal(5000000),
            BigDecimal(5000000)

    )

    val researches_6 = Researches(6,
            "Heavy Warship",

            R.drawable.res61,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Deuter",
            BigDecimal(10000000),
            BigDecimal(10000000),


            R.drawable.res62,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Lead",
            BigDecimal(20000000),
            BigDecimal(20000000),

            R.drawable.res63,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "N1",
            BigDecimal(30000000),
            BigDecimal(30000000),

            R.drawable.res64,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "NitroN1",
            BigDecimal(40000000),
            BigDecimal(40000000),

            R.drawable.res65,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Nuke",
            BigDecimal(50000000),
            BigDecimal(50000000)

    )

    val researches_7 = Researches(7,
            "Plasma Chaser",

            R.drawable.res71,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Plasma",
            BigDecimal(100000000),
            BigDecimal(100000000),


            R.drawable.res72,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Nano",
            BigDecimal(200000000),
            BigDecimal(200000000),

            R.drawable.res73,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "P1",
            BigDecimal(300000000),
            BigDecimal(300000000),

            R.drawable.res74,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "NitroP1",
            BigDecimal(400000000),
            BigDecimal(400000000),

            R.drawable.res75,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "XPlama",
            BigDecimal(500000000),
            BigDecimal(500000000)

    )

    val researches_8 = Researches(48,
            "White Star",

            R.drawable.res81,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "Flash",
            BigDecimal(1000000000),
            BigDecimal(1000000000),


            R.drawable.res82,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "XPhoton",
            BigDecimal(2000000000),
            BigDecimal(2000000000),

            R.drawable.res83,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "P1",
            BigDecimal(3000000000),
            BigDecimal(3000000000),

            R.drawable.res84,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "NitroP1",
            BigDecimal(4000000000),
            BigDecimal(4000000000),

            R.drawable.res85,
            BigDecimal(0),
            BigDecimal(0.00).setScale(2, BigDecimal.ROUND_DOWN),
            "XLight",
            BigDecimal(5000000000),
            BigDecimal(5000000000)

    )

    val  researches_table_const = arrayListOf<Researches>(

            researches_1,
            researches_2,
            researches_3,
            researches_4,
            researches_5,
            researches_6,
            researches_7,
            researches_8
            )

    companion object {

        val instance: ResearchesListSingleton  by lazy{ ResearchesListSingleton() }
    }

    fun add_initial_research(research: Researches) {

        if(this.research_table.size==0) {
            this.research_table.add(research)
        }
    }

    fun add_researches(position:Int) {

        if((position==this.research_table.size)&&(this.researches_table_const.size > position)) {
            this.research_table.add(this.researches_table_const[position])
        }
    }

    fun update_researches_to_DB(dbhandler : MyDBHandler){

        var i = 0;

        for(worker in this.research_table){
            dbhandler.addResearches(worker)
            dbhandler.updateResearches(worker,i)
            i++
        }

    }

    fun update_researches_table_from_DB(dbhandler : MyDBHandler){
        var research: Researches

        val data = dbhandler.getResearchesListContents()
        val numRows = data.count

        if (numRows==0){

        }else{

            var x =0
            while (x < this.research_table.size){
                this.research_table.removeAt(x)
                x++
                }

            var i = 0;
            while(data.moveToNext()){
                research = Researches(data.getString(0).toInt(),
                        data.getString(1),
                        researches_table_const[i].image_technology,
                        BigDecimal(data.getString(2)),
                        BigDecimal(data.getString(3)),
                        data.getString(4),
                        BigDecimal(data.getString(5)),
                        BigDecimal(data.getString(6)),

                        researches_table_const[i].image_body,
                        BigDecimal(data.getString(7)),
                        BigDecimal(data.getString(8)),
                        data.getString(9),
                        BigDecimal(data.getString(10)),
                        BigDecimal(data.getString(11)),

                        researches_table_const[i].image_engine,
                        BigDecimal(data.getString(12)),
                        BigDecimal(data.getString(13)),
                        data.getString(14),
                        BigDecimal(data.getString(15)),
                        BigDecimal(data.getString(16)),

                        researches_table_const[i].image_turbo,
                        BigDecimal(data.getString(17)),
                        BigDecimal(data.getString(18)),
                        data.getString(19),
                        BigDecimal(data.getString(20)),
                        BigDecimal(data.getString(21)),

                        researches_table_const[i].image_weapon,
                        BigDecimal(data.getString(22)),
                        BigDecimal(data.getString(23)),
                        data.getString(24),
                        BigDecimal(data.getString(25)),
                        BigDecimal(data.getString(26))

                )






                this.research_table.add(i,research)
                ++i


            }

        }
    }
}