package bazar666.uraniumclicker


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import bazar666.uraniumclicker.R


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Fragment3 : Fragment() {

    private lateinit var listView: ListView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment3, container, false)
        val  researches_table = ResearchesListSingleton.instance.research_table
        val adapter = MyResearchesListAdapter(context, researches_table)
        // Inflate the layout for this fragment

        listView = rootView.findViewById(R.id.research_list)
        listView.adapter = adapter

        listView.setOnItemClickListener {

            parent, view, position, id ->
            //Toast.makeText(context,  position.toString(), Toast.LENGTH_LONG).show()
        }

        return return rootView


    }

    override fun onDestroy() {
        super.onDestroy()
    }



}
