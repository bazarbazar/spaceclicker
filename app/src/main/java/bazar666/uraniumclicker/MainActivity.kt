package bazar666.uraniumclicker

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*
import com.startapp.android.publish.adsCommon.StartAppAd
import com.startapp.android.publish.adsCommon.StartAppSDK

class MainActivity : AppCompatActivity() {
    private lateinit var mp: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        StartAppSDK.init(this, "208033043", true);
        mp = MediaPlayer.create (this, R.raw.spacemusic)
        mp.start ()
        mp.isLooping = true

        toggleButton.isChecked=false
        toggleButton.setBackgroundResource(R.drawable.speakers)
        (toggleButton.background as AnimationDrawable).start()
        toggleButton.setOnCheckedChangeListener{
            buttonView, isChecked ->
            if (isChecked) {
                // The toggle is enabled/checked
                mp.pause()

                (toggleButton.background as AnimationDrawable).stop()
            } else {
                // The toggle is disabled
                mp.start()
                (toggleButton.background as AnimationDrawable).start()
            }


        }
        var buttonSTART = findViewById<ImageView>(R.id.buttonSTART)
        buttonSTART.setBackgroundResource(R.drawable.button_start);
        (buttonSTART.background as AnimationDrawable).start()

        buttonSTART.setOnClickListener(){
        val intent = Intent(this,Main2Activity::class.java)
        startActivity(intent)
        //StartAppAd.showAd(this)
        }



        var image = findViewById<ImageView>(R.id.background_main)
        image.setBackgroundResource(R.drawable.backround_main);
       // (image.background as AnimationDrawable).start()


    }



}
