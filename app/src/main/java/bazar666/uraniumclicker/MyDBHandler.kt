package bazar666.uraniumclicker


import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.icu.math.BigDecimal


class MyDBHandler(context: Context, name: String?,
                                      factory: SQLiteDatabase.CursorFactory?, version: Int) :
        SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {


    companion object {
      //  lateinit var MyDBHandler.Conte
        private var DATABASE_VERSION = 1
        private var DATABASE_NAME = "clicker.db"
//-----------------------WORKERS-----------------------------
        var TABLE_WORKERS = "workers"
        var COLUMN_ID = "_id"
        var COLUMN_WORKERNAME = "workerName"
        var COLUMN_WORKERINCOMECASH = "workerIncomeCash"
        var COLUMN_WORKERPRICE = "workerPrice"
        var COLUMN_WORKERPRICEMOD = "workerPriceMod"
        var COLUMN_WORKERUPGRADEPRICE = "workerUpgradePrice"
        var COLUMN_WORKERAMOUNT = "workerAmount"
        var COLUMN_WORKERINCOMECASHMOD = "workerIncomeCashMOD"
//-----------------------COMPANY-----------------------------
        var TABLE_COMPANY = "company"
        var COLUMN_COMPANYNAME = "companyName"
        var COLUMN_COMPANYCASH = "companyCash"
        var COLUMN_COMPANYCASHPERSEC = "companyCashPerSec"
//-----------------------RESEARCHES-----------------------------
        var TABLE_RESEARCHES = "researches"
        var COLUMN_RESEARCHES_SHIP_NAME = "researchesShipName"

        var COLUMN_RESEARCHES_LEVEL_TECHNOLOGY = "researchesLevelTechnology"
        var COLUMN_RESEARCHES_INCOME_CASH_MOD_TECHNOLOGY = "researchesIncomeCashModTechnology"
        var COLUMN_RESEARCHES_RESEARCH_NAME_TECHNOLOGY = "researchesResearchNameTechnology"
        var COLUMN_RESEARCHES_PRICE_MOD_TECHNOLOGY = "researchesPriceModTechnology"
        var COLUMN_RESEARCHES_UPGRADE_PRICE_TECHNOLOGY = "researchesUpgradePriceTechnology"

        var COLUMN_RESEARCHES_LEVEL_BODY = "researchesLevelBody"
        var COLUMN_RESEARCHES_INCOME_CASH_MOD_BODY = "researchesIncomeCashModBody"
        var COLUMN_RESEARCHES_RESEARCH_NAME_BODY = "researchesResearchNameBody"
        var COLUMN_RESEARCHES_PRICE_MOD_BODY = "researchesPriceModBody"
        var COLUMN_RESEARCHES_UPGRADE_PRICE_BODY = "researchesUpgradePriceBody"

        var COLUMN_RESEARCHES_LEVEL_ENGINE = "researchesLevelEngine"
        var COLUMN_RESEARCHES_INCOME_CASH_MOD_ENGINE = "researchesIncomeCashModEngine"
        var COLUMN_RESEARCHES_RESEARCH_NAME_ENGINE = "researchesResearchNameEngine"
        var COLUMN_RESEARCHES_PRICE_MOD_ENGINE = "researchesPriceModEngine"
        var COLUMN_RESEARCHES_UPGRADE_PRICE_ENGINE = "researchesUpgradePriceEngine"

        var COLUMN_RESEARCHES_LEVEL_TURBO = "researchesLevelTurbo"
        var COLUMN_RESEARCHES_INCOME_CASH_MOD_TURBO = "researchesIncomeCashModTurbo"
        var COLUMN_RESEARCHES_RESEARCH_NAME_TURBO = "researchesResearchNameTurbo"
        var COLUMN_RESEARCHES_PRICE_MOD_TURBO = "researchesPriceModTurbo"
        var COLUMN_RESEARCHES_UPGRADE_PRICE_TURBO = "researchesUpgradePriceTurbo"

        var COLUMN_RESEARCHES_LEVEL_WEAPON = "researchesLevelWeapon"
        var COLUMN_RESEARCHES_INCOME_CASH_MOD_WEAPON = "researchesIncomeCashModWeapon"
        var COLUMN_RESEARCHES_RESEARCH_NAME_WEAPON = "researchesResearchNameWeapon"
        var COLUMN_RESEARCHES_PRICE_MOD_WEAPON = "researchesPriceModWeapon"
        var COLUMN_RESEARCHES_UPGRADE_PRICE_WEAPON = "researchesUpgradePriceWeapon"


       // val instance: MyDBHandler by lazy{ MyDBHandler(this ,null, null, 1)}
    }



    override fun onCreate(db: SQLiteDatabase){
        val CREATE_WORKERS_TABLE = ("CREATE TABLE " +
                TABLE_WORKERS + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_WORKERNAME + " TEXT  unique," +
                COLUMN_WORKERINCOMECASH + " TEXT," +
                COLUMN_WORKERPRICE + " TEXT," +
                COLUMN_WORKERPRICEMOD + " TEXT," +
                COLUMN_WORKERUPGRADEPRICE + " TEXT," +
                COLUMN_WORKERAMOUNT + " TEXT," +
                COLUMN_WORKERINCOMECASHMOD + " TEXT" +")")
        db.execSQL(CREATE_WORKERS_TABLE)

        val CREATE_COMPANY_TABLE = ("CREATE TABLE " +
                TABLE_COMPANY + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_COMPANYNAME + " TEXT  unique," +
                COLUMN_COMPANYCASH + " TEXT," +
                COLUMN_COMPANYCASHPERSEC + " TEXT" +")")
        db.execSQL(CREATE_COMPANY_TABLE)

        val CREATE_RESEARCHES_TABLE = ("CREATE TABLE " +
                TABLE_RESEARCHES + "("
                + COLUMN_ID + " INTEGER PRIMARY KEY," +

                COLUMN_RESEARCHES_SHIP_NAME + " TEXT  unique," +

                COLUMN_RESEARCHES_LEVEL_TECHNOLOGY + " TEXT," +
                COLUMN_RESEARCHES_INCOME_CASH_MOD_TECHNOLOGY + " TEXT," +
                COLUMN_RESEARCHES_RESEARCH_NAME_TECHNOLOGY + " TEXT," +
                COLUMN_RESEARCHES_PRICE_MOD_TECHNOLOGY + " TEXT," +
                COLUMN_RESEARCHES_UPGRADE_PRICE_TECHNOLOGY + " TEXT," +

                COLUMN_RESEARCHES_LEVEL_BODY + " TEXT," +
                COLUMN_RESEARCHES_INCOME_CASH_MOD_BODY + " TEXT," +
                COLUMN_RESEARCHES_RESEARCH_NAME_BODY + " TEXT," +
                COLUMN_RESEARCHES_PRICE_MOD_BODY + " TEXT," +
                COLUMN_RESEARCHES_UPGRADE_PRICE_BODY + " TEXT," +

                COLUMN_RESEARCHES_LEVEL_ENGINE + " TEXT," +
                COLUMN_RESEARCHES_INCOME_CASH_MOD_ENGINE + " TEXT," +
                COLUMN_RESEARCHES_RESEARCH_NAME_ENGINE + " TEXT," +
                COLUMN_RESEARCHES_PRICE_MOD_ENGINE + " TEXT," +
                COLUMN_RESEARCHES_UPGRADE_PRICE_ENGINE + " TEXT," +

                COLUMN_RESEARCHES_LEVEL_TURBO + " TEXT," +
                COLUMN_RESEARCHES_INCOME_CASH_MOD_TURBO + " TEXT," +
                COLUMN_RESEARCHES_RESEARCH_NAME_TURBO + " TEXT," +
                COLUMN_RESEARCHES_PRICE_MOD_TURBO + " TEXT," +
                COLUMN_RESEARCHES_UPGRADE_PRICE_TURBO + " TEXT," +

                COLUMN_RESEARCHES_LEVEL_WEAPON + " TEXT," +
                COLUMN_RESEARCHES_INCOME_CASH_MOD_WEAPON + " TEXT," +
                COLUMN_RESEARCHES_RESEARCH_NAME_WEAPON + " TEXT," +
                COLUMN_RESEARCHES_PRICE_MOD_WEAPON + " TEXT," +
                COLUMN_RESEARCHES_UPGRADE_PRICE_WEAPON + " TEXT" +")")

        db.execSQL(CREATE_RESEARCHES_TABLE)





    }




    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int){

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORKERS)
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPANY)
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESEARCHES)
        onCreate(db)
    }
    fun addWorker(worker: Worker){
        val values = ContentValues()
        values.put(COLUMN_WORKERNAME,worker.name)
        values.put(COLUMN_WORKERINCOMECASH,worker.income_cash.toString())
        values.put(COLUMN_WORKERPRICE,worker.price.toString())
        values.put(COLUMN_WORKERPRICEMOD,worker.price_mod.toString())
        values.put(COLUMN_WORKERUPGRADEPRICE,worker.upgrade_price.toString())
        values.put(COLUMN_WORKERAMOUNT,worker.amount.toString())
        values.put(COLUMN_WORKERINCOMECASHMOD,worker.income_cash_mod.toString())

        val db = this.writableDatabase

        db.insert(TABLE_WORKERS, null, values)
        db.close()

    }
    fun createCompany(company: Company){
        val values = ContentValues()
        values.put(COLUMN_COMPANYNAME,company.company_name)
        values.put(COLUMN_COMPANYCASH,company.cash.toString())
        values.put(COLUMN_COMPANYCASHPERSEC,company.cash_per_sec.toString())

        val db = this.writableDatabase

        db.insert(TABLE_COMPANY, null, values)
        db.close()

    }

    fun updateCompany(company: Company){
        val values = ContentValues()
        //values.put(COLUMN_WORKERNAME,worker.name)
        values.put(COLUMN_COMPANYCASH,company.cash.toString())
        values.put(COLUMN_COMPANYCASHPERSEC,company.cash_per_sec.toString())

        val db = this.writableDatabase
        db.update(TABLE_COMPANY,values,  COLUMN_ID + "=" +(1) ,null)
        db.close()
    }

    fun updateCompanyCash(cash: BigDecimal){
        val values = ContentValues()
        values.put(COLUMN_COMPANYCASH,cash.toString())
        val db = this.writableDatabase
        db.update(TABLE_COMPANY,values,  COLUMN_COMPANYNAME + "='company'" ,null)
        db.close()
    }
    fun updateCompanyCPS(cash: BigDecimal){
        val values = ContentValues()
        values.put(COLUMN_COMPANYCASHPERSEC,cash.toString())
        val db = this.writableDatabase
        db.update(TABLE_COMPANY,values,  COLUMN_COMPANYNAME + "='company'" ,null)
        db.close()
    }

    fun updateWorker(worker: Worker, position:Int){
        val values = ContentValues()
        //values.put(COLUMN_WORKERNAME,worker.name)
        values.put(COLUMN_WORKERINCOMECASH,worker.income_cash.toString())
        values.put(COLUMN_WORKERPRICE,worker.price.toString())
        values.put(COLUMN_WORKERPRICEMOD,worker.price_mod.toString())
        values.put(COLUMN_WORKERUPGRADEPRICE,worker.upgrade_price.toString())
        values.put(COLUMN_WORKERAMOUNT,worker.amount.toString())
        values.put(COLUMN_WORKERINCOMECASHMOD,worker.income_cash_mod.toString())
        val db = this.writableDatabase
        db.update(TABLE_WORKERS,values,  COLUMN_ID + "=" +(position+1) ,null)
        db.close()
    }

    fun getListContents(): Cursor {
        val db = this.writableDatabase
        val values = db.rawQuery("SELECT * FROM " + TABLE_WORKERS, null)

        return  values
    }

    fun getResearchesListContents(): Cursor {
        val db = this.writableDatabase
        val values = db.rawQuery("SELECT * FROM " + TABLE_RESEARCHES, null)

        return  values
    }

    fun getWorkerContentsId(id:Int): Cursor {
        val db = this.writableDatabase
        val values = db.rawQuery("SELECT * FROM " + TABLE_WORKERS +" WHERE " + COLUMN_ID + "=" +(id+1) , null)

        return  values
    }

    fun getCompanyContents(): Cursor {
        val db = this.writableDatabase
        val values = db.rawQuery("SELECT * FROM " + TABLE_COMPANY, null)

        return  values
    }

    fun addResearches(research: Researches) {
        val values = ContentValues()

                values.put(COLUMN_RESEARCHES_SHIP_NAME,research.ship_name)

                values.put(COLUMN_RESEARCHES_LEVEL_TECHNOLOGY,research.level_technology.toString())
                values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_TECHNOLOGY,research.income_cash_mod_technology.toString())
                values.put(COLUMN_RESEARCHES_RESEARCH_NAME_TECHNOLOGY,research.research_name_technology)
                values.put(COLUMN_RESEARCHES_PRICE_MOD_TECHNOLOGY,research.price_mod_technology.toString())
                values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_TECHNOLOGY,research.upgrade_price_technology.toString())

                values.put(COLUMN_RESEARCHES_LEVEL_BODY,research.level_body.toString())
                values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_BODY,research.income_cash_mod_body.toString())
                values.put(COLUMN_RESEARCHES_RESEARCH_NAME_BODY,research.research_name_body)
                values.put(COLUMN_RESEARCHES_PRICE_MOD_BODY,research.price_mod_body.toString())
                values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_BODY,research.upgrade_price_body.toString())

                values.put(COLUMN_RESEARCHES_LEVEL_ENGINE,research.level_engine.toString())
                values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_ENGINE,research.income_cash_mod_engine.toString())
                values.put(COLUMN_RESEARCHES_RESEARCH_NAME_ENGINE,research.research_name_engine)
                values.put(COLUMN_RESEARCHES_PRICE_MOD_ENGINE,research.price_mod_engine.toString())
                values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_ENGINE,research.upgrade_price_engine.toString())

                values.put(COLUMN_RESEARCHES_LEVEL_TURBO,research.level_turbo.toString())
                values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_TURBO,research.income_cash_mod_turbo.toString())
                values.put(COLUMN_RESEARCHES_RESEARCH_NAME_TURBO,research.research_name_turbo)
                values.put(COLUMN_RESEARCHES_PRICE_MOD_TURBO,research.price_mod_turbo.toString())
                values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_TURBO,research.upgrade_price_turboe.toString())

                values.put(COLUMN_RESEARCHES_LEVEL_WEAPON,research.level_weapon.toString())
                values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_WEAPON,research.income_cash_mod_weapon.toString())
                values.put(COLUMN_RESEARCHES_RESEARCH_NAME_WEAPON,research.research_name_weapon)
                values.put(COLUMN_RESEARCHES_PRICE_MOD_WEAPON,research.price_mod_weapon.toString())
                values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_WEAPON,research.upgrade_price_weapon.toString())

        val db = this.writableDatabase

        db.insert(TABLE_RESEARCHES, null, values)
        db.close()

    }

    fun updateResearches(research: Researches, position: Int) {
        val values = ContentValues()

        values.put(COLUMN_RESEARCHES_SHIP_NAME,research.ship_name)

        values.put(COLUMN_RESEARCHES_LEVEL_TECHNOLOGY,research.level_technology.toString())
        values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_TECHNOLOGY,research.income_cash_mod_technology.toString())
        values.put(COLUMN_RESEARCHES_RESEARCH_NAME_TECHNOLOGY,research.research_name_technology)
        values.put(COLUMN_RESEARCHES_PRICE_MOD_TECHNOLOGY,research.price_mod_technology.toString())
        values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_TECHNOLOGY,research.upgrade_price_technology.toString())

        values.put(COLUMN_RESEARCHES_LEVEL_BODY,research.level_body.toString())
        values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_BODY,research.income_cash_mod_body.toString())
        values.put(COLUMN_RESEARCHES_RESEARCH_NAME_BODY,research.research_name_body)
        values.put(COLUMN_RESEARCHES_PRICE_MOD_BODY,research.price_mod_body.toString())
        values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_BODY,research.upgrade_price_body.toString())

        values.put(COLUMN_RESEARCHES_LEVEL_ENGINE,research.level_engine.toString())
        values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_ENGINE,research.income_cash_mod_engine.toString())
        values.put(COLUMN_RESEARCHES_RESEARCH_NAME_ENGINE,research.research_name_engine)
        values.put(COLUMN_RESEARCHES_PRICE_MOD_ENGINE,research.price_mod_engine.toString())
        values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_ENGINE,research.upgrade_price_engine.toString())

        values.put(COLUMN_RESEARCHES_LEVEL_TURBO,research.level_turbo.toString())
        values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_TURBO,research.income_cash_mod_turbo.toString())
        values.put(COLUMN_RESEARCHES_RESEARCH_NAME_TURBO,research.research_name_turbo)
        values.put(COLUMN_RESEARCHES_PRICE_MOD_TURBO,research.price_mod_turbo.toString())
        values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_TURBO,research.upgrade_price_turboe.toString())

        values.put(COLUMN_RESEARCHES_LEVEL_WEAPON,research.level_weapon.toString())
        values.put(COLUMN_RESEARCHES_INCOME_CASH_MOD_WEAPON,research.income_cash_mod_weapon.toString())
        values.put(COLUMN_RESEARCHES_RESEARCH_NAME_WEAPON,research.research_name_weapon)
        values.put(COLUMN_RESEARCHES_PRICE_MOD_WEAPON,research.price_mod_weapon.toString())
        values.put(COLUMN_RESEARCHES_UPGRADE_PRICE_WEAPON,research.upgrade_price_weapon.toString())
        val db = this.writableDatabase
        db.update(TABLE_RESEARCHES,values,  COLUMN_ID + "=" +(position+1) ,null)
        db.close()
    }


}