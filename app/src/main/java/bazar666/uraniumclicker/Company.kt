package bazar666.uraniumclicker

import java.math.BigDecimal

class Company private constructor(var cash: BigDecimal) {
    val company_name = "company"
    var cash_per_sec: BigDecimal = BigDecimal(0.00)
    companion object {

        val instance: Company by lazy{ Company(BigDecimal(0.00)) }
    }


    fun create_company(dbhandler : MyDBHandler): Company {

        val company = instance
        dbhandler.createCompany(company)
        val data = dbhandler.getListContents()
        val numRows = data.count

        if (numRows==0){
            company.cash = BigDecimal(data.getString(1))
            company.cash_per_sec = BigDecimal(data.getString(2))

        }
        return company
    }


    fun update_company_to_DB(dbhandler : MyDBHandler){

        val company = instance




        dbhandler.updateCompany(company)



    }

    fun update_company_from_DB(dbhandler : MyDBHandler) {


        val data = dbhandler.getCompanyContents()
        val numRows = data.count

        if (numRows == 0) {

        } else {
            data.moveToPosition(0)
            this.cash = BigDecimal(data.getString(2))
            this.cash_per_sec = BigDecimal(data.getString(3))

        }
    }
}