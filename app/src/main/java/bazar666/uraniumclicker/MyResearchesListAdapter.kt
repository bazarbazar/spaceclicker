package bazar666.uraniumclicker

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import java.math.BigDecimal
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import bazar666.uraniumclicker.R


class MyResearchesListAdapter (context: Context?, private val values: ArrayList<Researches>) : ArrayAdapter<Researches>(context, -1, values) {
    fun show_value_text_perecent(cash: BigDecimal, textView: TextView) {
        var cash_tmp = cash

        if (cash >= BigDecimal("1000000000000")) {
            textView.setText("+" +cash_tmp.divide(BigDecimal("1000000"), 2, BigDecimal.ROUND_DOWN).toString() + "B%")

        } else if (cash >= BigDecimal("1000000000")) {
            textView.setText("+" +cash_tmp.divide(BigDecimal("1000000"), 2, BigDecimal.ROUND_DOWN).toString() + "Mi%")


        } else if (cash >= BigDecimal("1000000")) {
            textView.setText("+" +cash_tmp.divide(BigDecimal("1000000"), 2, BigDecimal.ROUND_DOWN).toString() + "M%")


        } else if (cash >= BigDecimal("1000")) {
            textView.setText("+" +cash_tmp.divide(BigDecimal("1000.00"), 2, BigDecimal.ROUND_DOWN).toString() + "K%")

        } else
            textView.setText("+" + cash_tmp.toString()+"%")
    }

    fun show_value_text_lvl(cash: BigDecimal, textView: TextView) {
        var cash_tmp = cash

        if (cash >= BigDecimal(1000000000000)) {
            textView.setText("LV" + cash_tmp.divide(BigDecimal("1000000"), 2, BigDecimal.ROUND_DOWN).toString() + "B")

        } else if (cash >= BigDecimal(1000000000)) {
            textView.setText("LV" + cash_tmp.divide(BigDecimal("1000000"), 2, BigDecimal.ROUND_DOWN).toString() + "Mi")


        } else if (cash >= BigDecimal(1000000)) {
            textView.setText("LV" + cash_tmp.divide(BigDecimal("1000000"), 2, BigDecimal.ROUND_DOWN).toString() + "M")


        } else if (cash >= BigDecimal(1000)) {
            textView.setText("LV" + cash_tmp.divide(BigDecimal("1000.00"), 2, BigDecimal.ROUND_DOWN).toString() + "K")

        } else
            textView.setText("LV" + cash_tmp.toString())
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {







        val company = Company.instance
        val  workers_table = WorkerListSingleton.instance.workers_table



        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.row_view_researches, parent, false)

        val ship_name = rowView.findViewById<TextView>(R.id.ship_name) as TextView
        ship_name.text = values[position].ship_name

        val res_name1 = rowView.findViewById<TextView>(R.id.res_name1) as TextView
        res_name1.text = values[position].research_name_technology

        val income_cash_mode1 = rowView.findViewById<TextView>(R.id.income_cash_mode1) as TextView
        income_cash_mode1.text = "+" + values[position].income_cash_mod_technology.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN).toString()+"%"

        val level1 = rowView.findViewById<TextView>(R.id.level1) as TextView
        level1.text = "LV " + values[position].level_technology.toString()

        val res_img1 = rowView.findViewById<ImageView>(R.id.res_img1) as ImageView
        res_img1.setImageResource(values[position].image_technology)
        val buy_res1 = rowView.findViewById<Button>(R.id.buy_res1)

        var image1 = rowView.findViewById<ImageView>(R.id.res_img11)
        image1.setBackgroundResource(R.drawable.ress_border_animation);



//============================2===========================

        val res_name2 = rowView.findViewById<TextView>(R.id.res_name2) as TextView
        res_name2.text = values[position].research_name_body

        val income_cash_mode2 = rowView.findViewById<TextView>(R.id.income_cash_mode2) as TextView
        income_cash_mode2.text = "+" + values[position].income_cash_mod_body.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN).toString()+"%"

        val level2 = rowView.findViewById<TextView>(R.id.level2) as TextView
        level2.text = "LV " + values[position].level_body.toString()

        val res_img2 = rowView.findViewById<ImageView>(R.id.res_img2) as ImageView
        res_img2.setImageResource(values[position].image_body)
        val buy_res2 = rowView.findViewById<Button>(R.id.buy_res2)

        var image2 = rowView.findViewById<ImageView>(R.id.res_img22)
        image2.setBackgroundResource(R.drawable.ress_border_animation);
//============================3===========================

        val res_name3 = rowView.findViewById<TextView>(R.id.res_name3) as TextView
        res_name3.text = values[position].research_name_engine

        val income_cash_mode3 = rowView.findViewById<TextView>(R.id.income_cash_mode3) as TextView
        income_cash_mode3.text = "+" + values[position].income_cash_mod_engine.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN).toString()+"%"

        val level3 = rowView.findViewById<TextView>(R.id.level3) as TextView
        level3.text = "LV " + values[position].level_engine.toString()

        val res_img3 = rowView.findViewById<ImageView>(R.id.res_img3) as ImageView
        res_img3.setImageResource(values[position].image_engine)
        val buy_res3 = rowView.findViewById<Button>(R.id.buy_res3)

        var image3 = rowView.findViewById<ImageView>(R.id.res_img33)
        image3.setBackgroundResource(R.drawable.ress_border_animation);
//============================4===========================

        val res_name4 = rowView.findViewById<TextView>(R.id.res_name4) as TextView
        res_name4.text = values[position].research_name_turbo

        val income_cash_mode4 = rowView.findViewById<TextView>(R.id.income_cash_mode4) as TextView
        income_cash_mode4.text = "+" + values[position].income_cash_mod_turbo.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN).toString()+"%"

        val level4 = rowView.findViewById<TextView>(R.id.level4) as TextView
        level4.text = "LV " + values[position].level_turbo.toString()

        val res_img4 = rowView.findViewById<ImageView>(R.id.res_img4) as ImageView
        res_img4.setImageResource(values[position].image_turbo)
        val buy_res4 = rowView.findViewById<Button>(R.id.buy_res4)

        var image4 = rowView.findViewById<ImageView>(R.id.res_img44)
        image4.setBackgroundResource(R.drawable.ress_border_animation);
//============================5===========================

        val res_name5 = rowView.findViewById<TextView>(R.id.res_name5) as TextView
        res_name5.text = values[position].research_name_weapon

        val income_cash_mode5 = rowView.findViewById<TextView>(R.id.income_cash_mode5) as TextView
        income_cash_mode5.text = "+" + values[position].income_cash_mod_weapon.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN).toString()+"%"

        val level5 = rowView.findViewById<TextView>(R.id.level5) as TextView
        level5.text = "LV " + values[position].level_weapon.toString()

        val res_img5 = rowView.findViewById<ImageView>(R.id.res_img5) as ImageView
        res_img5.setImageResource(values[position].image_weapon)
        val buy_res5 = rowView.findViewById<Button>(R.id.buy_res5)

        var image5 = rowView.findViewById<ImageView>(R.id.res_img55)
        image5.setBackgroundResource(R.drawable.ress_border_animation);

        buy_res1.setOnClickListener(object : View.OnClickListener{


            override fun onClick(v: View?) {

                //Toast.makeText(context, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                if (company.cash >= values[position].upgrade_price_technology) {

                    company.cash = company.cash.subtract(values[position].upgrade_price_technology)

                    values[position].level_technology = values[position].level_technology.add(BigDecimal(1))
                    values[position].upgrade_price_technology =  values[position].upgrade_price_technology.add(values[position].price_mod_technology)
                    values[position].income_cash_mod_technology=values[position].income_cash_mod_technology.add(BigDecimal(0.01).setScale(2,BigDecimal.ROUND_DOWN))
                    workers_table[position].income_cash_mod=workers_table[position].income_cash_mod.add(BigDecimal(0.01).setScale(2,BigDecimal.ROUND_DOWN))
                    company.cash_per_sec = company.cash_per_sec.add( (workers_table[position].income_cash.multiply(BigDecimal(0.01)).multiply(workers_table[position].amount).setScale(2,BigDecimal.ROUND_DOWN)) )

                    show_value_text_perecent(values[position].income_cash_mod_technology.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN),income_cash_mode1)
                    show_value_text_lvl(values[position].level_technology,level1)



                }
            }
        })

        buy_res2.setOnClickListener(object : View.OnClickListener{


            override fun onClick(v: View?) {

                //Toast.makeText(context, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                if (company.cash >= values[position].upgrade_price_body) {

                    company.cash = company.cash.subtract(values[position].upgrade_price_body)

                    values[position].level_body = values[position].level_body.add(BigDecimal(1))
                    values[position].upgrade_price_body =  values[position].upgrade_price_body.add(values[position].price_mod_body)
                    values[position].income_cash_mod_body=values[position].income_cash_mod_body.add(BigDecimal(0.02).setScale(2,BigDecimal.ROUND_DOWN))
                    workers_table[position].income_cash_mod=workers_table[position].income_cash_mod.add(BigDecimal(0.02).setScale(2,BigDecimal.ROUND_DOWN))
                    company.cash_per_sec = company.cash_per_sec.add( (workers_table[position].income_cash.multiply(BigDecimal(0.02)).multiply(workers_table[position].amount).setScale(2,BigDecimal.ROUND_DOWN)) )

                    show_value_text_perecent(values[position].income_cash_mod_body.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN),income_cash_mode2)
                    show_value_text_lvl(values[position].level_body,level2)



                }
            }
        })
        buy_res3.setOnClickListener(object : View.OnClickListener{


            override fun onClick(v: View?) {

                //Toast.makeText(context, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                if (company.cash >= values[position].upgrade_price_engine) {

                    company.cash = company.cash.subtract(values[position].upgrade_price_engine)

                    values[position].level_engine = values[position].level_engine.add(BigDecimal(1))
                    values[position].upgrade_price_engine =  values[position].upgrade_price_engine.add(values[position].price_mod_engine)
                    values[position].income_cash_mod_engine=values[position].income_cash_mod_engine.add(BigDecimal(0.03).setScale(2,BigDecimal.ROUND_CEILING))
                    workers_table[position].income_cash_mod=workers_table[position].income_cash_mod.add(BigDecimal(0.03).setScale(2,BigDecimal.ROUND_CEILING))
                    company.cash_per_sec = company.cash_per_sec.add( (workers_table[position].income_cash.multiply(BigDecimal(0.03)).multiply(workers_table[position].amount).setScale(2,BigDecimal.ROUND_DOWN)) )

                    show_value_text_perecent(values[position].income_cash_mod_engine.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_CEILING),income_cash_mode3)
                    show_value_text_lvl(values[position].level_engine,level3)



                }
            }
        })

        buy_res4.setOnClickListener(object : View.OnClickListener{


            override fun onClick(v: View?) {

                //Toast.makeText(context, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                if (company.cash >= values[position].upgrade_price_turboe) {

                    company.cash = company.cash.subtract(values[position].upgrade_price_turboe)

                    values[position].level_turbo = values[position].level_turbo.add(BigDecimal(1))
                    values[position].upgrade_price_turboe =  values[position].upgrade_price_turboe.add(values[position].price_mod_turbo)
                    values[position].income_cash_mod_turbo=values[position].income_cash_mod_turbo.add(BigDecimal(0.04).setScale(2,BigDecimal.ROUND_DOWN))
                    workers_table[position].income_cash_mod=workers_table[position].income_cash_mod.add(BigDecimal(0.04).setScale(2,BigDecimal.ROUND_DOWN))
                    company.cash_per_sec = company.cash_per_sec.add( (workers_table[position].income_cash.multiply(BigDecimal(0.04)).multiply(workers_table[position].amount).setScale(2,BigDecimal.ROUND_DOWN)) )

                    show_value_text_perecent(values[position].income_cash_mod_turbo.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN),income_cash_mode4)
                    show_value_text_lvl(values[position].level_turbo,level4)



                }
            }
        })

        buy_res5.setOnClickListener(object : View.OnClickListener{


            override fun onClick(v: View?) {

                //Toast.makeText(context, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                if (company.cash >= values[position].upgrade_price_weapon) {

                    company.cash = company.cash.subtract(values[position].upgrade_price_weapon)

                    values[position].level_weapon = values[position].level_weapon.add(BigDecimal(1))
                    values[position].upgrade_price_weapon =  values[position].upgrade_price_weapon.add(values[position].price_mod_weapon)
                    values[position].income_cash_mod_weapon=values[position].income_cash_mod_weapon.add(BigDecimal(0.05).setScale(2,BigDecimal.ROUND_DOWN))
                    workers_table[position].income_cash_mod=workers_table[position].income_cash_mod.add(BigDecimal(0.05).setScale(2,BigDecimal.ROUND_DOWN))
                    company.cash_per_sec = company.cash_per_sec.add( (workers_table[position].income_cash.multiply(BigDecimal(0.05)).multiply(workers_table[position].amount).setScale(2,BigDecimal.ROUND_DOWN)) )

                    show_value_text_perecent(values[position].income_cash_mod_weapon.multiply(BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN),income_cash_mode5)
                    show_value_text_lvl(values[position].level_weapon,level5)



                }
            }
        })

        fun refresh_researches() {
            //buy_worker.isEnabled = false
            show_value_text(values[position].upgrade_price_technology, buy_res1, 2)
            show_value_text(values[position].upgrade_price_body, buy_res2, 2)
            show_value_text(values[position].upgrade_price_engine, buy_res3, 2)
            show_value_text(values[position].upgrade_price_turboe, buy_res4, 2)
            show_value_text(values[position].upgrade_price_weapon, buy_res5, 2)
            if (company.cash >= values[position].upgrade_price_technology) {
                buy_res1.isEnabled = true
                (image1.background as AnimationDrawable).start()

            } else {
                buy_res1.isEnabled = false
                (image1.background as AnimationDrawable).stop()
            }

            if (company.cash >= values[position].upgrade_price_body) {
                buy_res2.isEnabled = true
                (image2.background as AnimationDrawable).start()
            } else {
                buy_res2.isEnabled = false
                (image2.background as AnimationDrawable).stop()
            }
            if (company.cash >= values[position].upgrade_price_engine) {
                buy_res3.isEnabled = true
                (image3.background as AnimationDrawable).start()
            } else {
                buy_res3.isEnabled = false
                (image3.background as AnimationDrawable).stop()
            }
            if (company.cash >= values[position].upgrade_price_turboe) {
                buy_res4.isEnabled = true
                (image4.background as AnimationDrawable).start()
            } else {
                buy_res4.isEnabled = false
                (image4.background as AnimationDrawable).stop()
            }
            if (company.cash >= values[position].upgrade_price_weapon) {
                buy_res5.isEnabled = true
                (image5.background as AnimationDrawable).start()
            } else {
                buy_res5.isEnabled = false
                (image5.background as AnimationDrawable).stop()
            }

        }

        val handler = Handler()
        val runnable = object : Runnable {
            override fun run() {
                //var dbhandler = MyDBHandler(context ,null, null, 1)

                refresh_researches()


                handler.postDelayed(this, 200)

            }
        }
        handler.postDelayed(runnable, 200);

        return rowView
    }

}