package bazar666.uraniumclicker


import android.os.Bundle
import android.support.v4.app.Fragment
import android.icu.math.BigDecimal
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import bazar666.uraniumclicker.R
import bazar666.uraniumclicker.R.id.parent


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */

class Fragment2 : Fragment() {

    private lateinit var listView: ListView
    lateinit var myWorker: Worker
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment


        MyDBHandler(context, null, null, 1)

        val rootView = inflater.inflate(R.layout.fragment2, container, false)






        val adapter = MyWorkerListAdapter(context, WorkerListSingleton.instance.workers_table)



        listView = rootView.findViewById(R.id.worker_list)
        listView.adapter = adapter

        listView.setOnItemClickListener {

            parent, view, position, id ->
            //Toast.makeText(context,  position.toString(), Toast.LENGTH_LONG).show()
        }

        return return rootView


    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
