package bazar666.uraniumclicker

import android.content.Context
import java.math.BigDecimal
import android.os.Handler
import android.transition.Slide
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import bazar666.uraniumclicker.R.id.buttonTurbo
import bazar666.uraniumclicker.R.id.buy_worker2
import android.view.Gravity
import android.widget.PopupWindow


fun show_value_text(cash: BigDecimal, textView: TextView, scale: Int) {
    var cashTmp = cash

    if (cash >= BigDecimal(1000000000000)) {
        textView.text = cashTmp.divide(BigDecimal("1000000000000"), scale, BigDecimal.ROUND_DOWN).toString() + "B"

    } else if (cash >= BigDecimal(1000000000)) {
        textView.setText(cashTmp.divide(BigDecimal("1000000000"), scale, BigDecimal.ROUND_DOWN).toString() + "Mi")


    } else if (cash >= BigDecimal(1000000)) {
        textView.setText(cashTmp.divide(BigDecimal("1000000"), scale, BigDecimal.ROUND_DOWN).toString() + "M")


    } else if (cash >= BigDecimal(1000)) {
        textView.setText(cashTmp.divide(BigDecimal("1000.00"), scale, BigDecimal.ROUND_DOWN).toString() + "K")

    } else
        textView.setText(cashTmp.setScale(scale,BigDecimal.ROUND_DOWN).toString())
}

    class MyWorkerListAdapter (context: Context?, private val values: ArrayList<Worker>) : ArrayAdapter<Worker>(context, -1, values) {

   //var mClickListener = listener;

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.row_view_ships, parent, false)

        val textView1 = rowView.findViewById<TextView>(R.id.worker_desc) as TextView
        textView1.text = values[position].name

        val textView2 = rowView.findViewById<TextView>(R.id.worker_amount) as TextView
        //textView2.text = values[position].amount.toString()
        show_value_text(values[position].amount, textView2, 0)

        val textView3 = rowView.findViewById<TextView>(R.id.dps_amount) as TextView
        //textView3.text = values[position].income_cash.multiply(values[position].amount).multiply(values[position].income_cash_mod).toString()
        show_value_text((values[position].income_cash.multiply(values[position].amount)).add(values[position].income_cash.multiply(values[position].amount).multiply(values[position].income_cash_mod)), textView3, 2)

        val buy_worker = rowView.findViewById<Button>(buy_worker2)

        val  buttonTurbo = rowView.findViewById<ToggleButton>(buttonTurbo)

        val ship_img = rowView.findViewById<ImageView>(R.id.ship_image) as ImageView
        ship_img.setImageResource(values[position].ship_image)


        //buy_worker.isEnabled = false
        val company = Company.instance
        MyDBHandler(context, null, null, 1)



        class TURBO_desc(var turbo_desc1 :String,
                         var turbo_desc2:String,
                         var turbo_desc3:String,
                         var turbo_desc4:String){

        }
        val turbo_desc1 = TURBO_desc("instant +250% U ship UPS",
                "instant +500% U ship UPS",
                "instant +750% U ship UPS",
                "instant +1000% U ship UPS")

        fun refresh_workers() {
            //buy_worker.isEnabled = false
            show_value_text(values[position].price, buy_worker, 2)

                if (company.cash >= values[position].price) {
                    buy_worker.isEnabled = true
                } else {
                    buy_worker.isEnabled = false
                }



        }
        val handler = Handler()
        val runnable = object : Runnable {
            override fun run() {
                //var dbhandler = MyDBHandler(context ,null, null, 1)

                refresh_workers()


                handler.postDelayed(this, 200)

            }
        }
        handler.postDelayed(runnable, 200);
        buy_worker.setOnClickListener(object : View.OnClickListener{


            override fun onClick(v: View?) {

                //Toast.makeText(context, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                if (company.cash >= values[position].price) {
                    //company.cash = company.cash.add(myWorker.income_cash).add(myWorker.income_cash.multiply(myWorker.amount).multiply(myWorker.income_cash_mod))
                    //dbhandler.updateCompanyCash(company.cash)
                    //show_value_text(company.cash,editTextKLIK)
                    company.cash = company.cash.subtract(values[position].price)
                    // show_value_text(company.cash,editTextKLIK)
                    values[position].amount = values[position].amount.add(BigDecimal(1))
                    values[position].price =  values[position].price.add(values[position].price_mod)
                    company.cash_per_sec = company.cash_per_sec.add( values[position].income_cash).add(values[position].income_cash.multiply( values[position].income_cash_mod))

                    show_value_text(values[position].amount, textView2, 0)
                    show_value_text((values[position].income_cash.multiply(values[position].amount)).add(values[position].income_cash.multiply(values[position].amount).multiply(values[position].income_cash_mod)), textView3, 2)
                    //textView2.text = values[position].amount.toString()
                    //textView3.text = values[position].income_cash.multiply(values[position].amount).toString()
                if(values[position].amount.equals(BigDecimal(1))){
                    WorkerListSingleton.instance.add_workers(position+1)
                    ResearchesListSingleton.instance.add_researches(position)
                }

                }
            }
        })
        buttonTurbo.isChecked=false
        buttonTurbo.setOnCheckedChangeListener { buttonView, isChecked ->



            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.popup_example,null)
            val textView25 = view.findViewById<TextView>(R.id.textView_desc25)
            val textView50 = view.findViewById<TextView>(R.id.textView_desc50)
            val textView75 = view.findViewById<TextView>(R.id.textView_desc75)
            val textView100 = view.findViewById<TextView>(R.id.textView_desc100)

            textView25.setText(turbo_desc1.turbo_desc1)
            textView50.setText(turbo_desc1.turbo_desc2)
            textView75.setText(turbo_desc1.turbo_desc3)
            textView100.setText(turbo_desc1.turbo_desc4)

            val popupWindow = PopupWindow(
                    view, // Custom view to show in popup window
                    LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                    LinearLayout.LayoutParams.WRAP_CONTENT // Window height
            )
            //popupWindow.setOutsideTouchable(true);
            popupWindow.setFocusable(true);
            popupWindow.setOnDismissListener {



                 buttonTurbo.isChecked=true
                buttonTurbo.isChecked=false
            }

            val slideIn = Slide()
            slideIn.slideEdge = Gravity.BOTTOM
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            popupWindow.exitTransition = slideOut
            if (isChecked) {
                popupWindow.dismiss()
                popupWindow.showAtLocation(
                        buttonView, // Location to display popup window
                        Gravity.BOTTOM, // Exact position of layout to display popup
                        0, // X offset
                        0 // Y offset
                )
                val buttonPopup = view.findViewById<Button>(R.id.button_close)
                //val tv = view.findViewById<TextView>(R.id.text_view)

                buttonPopup.setOnClickListener {
                    // Dismiss the popup window
                    popupWindow.dismiss()
                    buttonTurbo.isChecked=false
            }

        }else{popupWindow.dismiss()
                }

                }

                // Set click listener for popup window's text view
                //tv.setOnClickListener{
                    // Change the text color of popup window's text view

                //}

                // Set a click listener for popup's button widget


                // Set a dismiss listener for popup window



                // Finally, show the popup window on app







        return rowView
    }









}